package service

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

func redirect(ctx *gin.Context, target string) {
	ctx.Redirect(http.StatusMovedPermanently, target)
}

func notFound(ctx *gin.Context) {
	ctx.JSON(http.StatusNotFound, gin.H{
		"status":  "not-found",
		"message": "Not Found",
	})
}

func success(ctx *gin.Context, data interface{}) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  "ok",
		"message": "Success",
		"result":  data,
	})
}

func failed(ctx *gin.Context, message string, err ...error) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  "failed",
		"message": message,
	})
}

func internalServerError(ctx *gin.Context, err error) {
	log.Println(err)
	ctx.JSON(http.StatusInternalServerError, gin.H{
		"status":  "internal-server-error",
		"message": "Internal Server Errror",
	})
}

func forbidden(ctx *gin.Context, msg string) {
	ctx.JSON(http.StatusForbidden, gin.H{
		"status":  "forbidden",
		"message": "Request Is Forbidden",
	})
}

module gitlab.com/kotahkon/webservice

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/dpapathanasiou/go-recaptcha v0.0.0-20190121160230-be5090b17804
	github.com/gin-gonic/gin v1.6.2
	github.com/jinzhu/now v1.0.1
	github.com/lib/pq v1.3.0
	github.com/nsqio/go-nsq v1.0.8
	github.com/vmihailenco/msgpack v4.0.4+incompatible
	gitlab.com/kotahkon/utilities v1.2.1
	google.golang.org/appengine v1.6.5 // indirect
)

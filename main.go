package main

import (
	"log"
	"runtime"

	"gitlab.com/kotahkon/webservice/service"
)

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU() + 1)
}

func main() {
	s, err := service.New()
	if err != nil {
		log.Fatal(err)
	}
	s.Run()
}

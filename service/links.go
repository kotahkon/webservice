package service

import (
	"time"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/now"

	"gitlab.com/kotahkon/utilities/database"
	"gitlab.com/kotahkon/utilities/model"
	"gitlab.com/kotahkon/utilities/validator"
)

func (s *Service) linksGetHandler(ctx *gin.Context) {
	user := s.getUserFromContext(ctx)

	var input struct {
		Search    string `form:"search"`
		Order     string `form:"order"`
		Direction string `form:"direction"`
	}
	err := ctx.BindQuery(&input)
	if err != nil {
		failed(ctx, "Bad query Input", err)
		return
	}
	output := []database.Target{}
	query := database.Model(database.TargetPtr).Order(input.Order+" "+input.Direction).Where("creator_id = ?", user.ID)
	if input.Search != "" {
		query = query.Where("short_id LIKE ? OR target_url LIKE ?", "%"+input.Search+"%", "%"+input.Search+"%")
	}
	query.Scan(&output)

	success(ctx, output)
}

func (s *Service) linksPostHandler(ctx *gin.Context) {
	var input struct {
		URL     string `json:"url"`
		ShortID string `json:"short_id"`
	}
	err := ctx.BindJSON(&input)
	if err != nil {
		failed(ctx, "Bad JSON Input", err)
		return
	}

	user := s.getUserFromContext(ctx)
	if !user.Exists() {
		forbidden(ctx, "You have to login")
		return
	}

	if !validator.Link(input.URL) {
		failed(ctx, "Invalid URL format")
		return
	}

	target := &database.Target{}
	target.CreatorID = user.ID
	target.TargetURL = input.URL
	if input.ShortID != "" {
		if !validator.ShortID(input.ShortID) {
			failed(ctx, "Invalid ShortID format")
			return
		}
		target.ShortID = input.ShortID
	}
	err = database.Model(target).Create(target).Error
	if err != nil {
		if err.Error() == `pq: duplicate key value violates unique constraint "targets_short_id_key"` {
			failed(ctx, "This short id isn't available, please try another.")
			return
		}
		internalServerError(ctx, err)
		return
	}
	model.SetTarget(target)

	success(ctx, target)
}

func (s *Service) linkGetHandler(ctx *gin.Context) {
	code := ctx.Param("code")
	target := model.GetTarget("short_id", code)
	if target == nil {
		notFound(ctx)
		return
	}

	user := s.getUserFromContext(ctx)
	if user.ID != target.CreatorID {
		forbidden(ctx, "Forbidden access")
		return
	}

	response := make([]int, 3)

	days := [][]time.Time{
		{
			now.BeginningOfDay(),
			time.Now(),
		},
		{
			now.BeginningOfDay().Add(-24 * time.Hour),
			now.BeginningOfDay(),
		},
		{
			now.BeginningOfDay().Add(-7 * 24 * time.Hour),
			now.BeginningOfDay(),
		},
	}

	for i := range days {
		day := days[i]

		database.Get().Raw(`
		SELECT count(vr.id)
		FROM visit_records AS vr 
		LEFT OUTER JOIN targets on targets.id = vr.target_id 
		WHERE targets.id = ? 
		AND vr.created_at BETWEEN ? AND ?;`, target.ID, day[0], day[1]).Count(&response[i])
	}

	success(ctx, response)
}

package service

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

var corsHeaders = []string{
	"Authorization",
	"Content-Type",
}

func CORS() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx.Header("Access-Control-Allow-Origin", "*")
		if ctx.Request.Method == "OPTIONS" {
			ctx.Header("Access-Control-Allow-Credentials", "true")
			ctx.Header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE")
			ctx.Writer.Header().Set("Access-Control-Allow-Headers", strings.Join(corsHeaders, ","))
			ctx.AbortWithStatus(http.StatusOK)
			return
		}
		ctx.Next()
	}
}

package service

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func (s *Service) newEngine() error {
	gin.SetMode(gin.ReleaseMode)
	s.engine = gin.New()

	s.engine.Use(CORS())
	s.engine.Use(gin.Logger())

	s.engine.NoMethod(func(ctx *gin.Context) {
		ctx.JSON(http.StatusMethodNotAllowed, gin.H{
			"status":  "method-not-allowed",
			"message": "Method Not Allowed",
		})
	})
	s.engine.NoRoute(func(ctx *gin.Context) {
		ctx.JSON(http.StatusNotFound, gin.H{
			"status":  "not-found",
			"message": "Route Not Found",
		})
	})

	auth := s.engine.Group("/auth")
	{
		auth.POST("/login", s.authLoginPostHandler)
		auth.POST("/register", s.authRegisterPostHandler)
		// TODO: refresh token
	}

	charts := s.engine.Group("/charts", s.userAuthMiddleware())
	{
		charts.GET("/overall", s.chartsOverallGetHandler)
		charts.GET("/link/:code/pie", s.chartsLinkPieGetHandler)
		charts.GET("/link/:code/line", s.chartsLinkLineGetHandler)
	}

	links := s.engine.Group("/links", s.userAuthMiddleware())
	{
		links.GET("", s.linksGetHandler)
		links.POST("", s.linksPostHandler)

		link := links.Group("/:code")
		{
			link.GET("", s.linkGetHandler)
			// TODO: DELETE method
		}
	}

	s.engine.GET("/r/:code", s.redirectGetHandler)
	return nil
}

package service

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/dpapathanasiou/go-recaptcha"
	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"

	"gitlab.com/kotahkon/utilities/cache"
	"gitlab.com/kotahkon/utilities/database"
)

type Service struct {
	engine *gin.Engine
}

func New() (*Service, error) {
	var err error

	secretKey := os.Getenv("SECRET_KEY")
	if secretKey == "" {
		return nil, errors.New("there is no SECRET_KEY in system environment")
	}

	captchaKey := os.Getenv("RECAPTCHA_PRIVATE_KEY")
	if captchaKey == "" {
		return nil, fmt.Errorf("there is no RECAPTCHA_PRIVATE_KEY in system environment")
	}
	recaptcha.Init(captchaKey)

	s := new(Service)
	err = s.newEngine()
	if err != nil {
		return nil, err
	}

	err = database.Open(os.Getenv("DATABASE_CONNECTION"))
	if err != nil {
		return nil, err
	}

	cache.New(os.Getenv("REDIS_ADDR"))
	err = cache.Ping()
	if err != nil {
		return nil, err
	}

	err = newProducer(os.Getenv("NSQ_CONNECTION"))
	if err != nil {
		return nil, err
	}

	return s, nil
}

func (s *Service) Run() {
	srv := &http.Server{
		Addr:           ":8090",
		Handler:        s.engine,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	go func() {
		log.Println("Listening on :8090 ...")
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit

	log.Println("Shuting down server...")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	producer.Stop()

	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server forced to shutdown:", err)
	}
	log.Println("Server exiting")
}

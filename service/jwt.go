package service

import (
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"gitlab.com/kotahkon/utilities/database"
	"gitlab.com/kotahkon/utilities/model"
)

type UserToken struct {
	UID string `json:"uid"`
	jwt.StandardClaims
}

func (s *Service) jwtGenerate(uid string) string {
	claims := UserToken{
		uid,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 6).Unix(),
			IssuedAt:  time.Now().Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	secretKey, _ := token.SignedString([]byte(os.Getenv("SECRET_KEY")))
	return secretKey
}

func parseToken(auth string, key string) *UserToken {
	out := &UserToken{}
	token, err := jwt.ParseWithClaims(auth, out, func(token *jwt.Token) (interface{}, error) {
		return []byte(key), nil
	})
	if err != nil {
		return nil
	}

	if claims, ok := token.Claims.(*UserToken); ok && token.Valid {
		return claims
	}
	return nil
}

func (s *Service) getUserFromContext(ctx *gin.Context) *database.User {
	user, exists := ctx.Get("user")
	if !exists {
		return nil
	}
	return user.(*database.User)
}

func (s *Service) userAuthMiddleware() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		auth := ctx.GetHeader("Authorization")
		if auth == "" {
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		parts := strings.Split(auth, " ")
		if len(parts) != 2 {
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		if parts[0] != "jwt" {
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		token := parseToken(parts[1], os.Getenv("SECRET_KEY"))
		if token == nil {
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		// check ExpiresAt
		if time.Now().After(time.Unix(token.ExpiresAt, 0)) {
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		user := model.GetUser("uid", token.UID)
		if !user.Exists() {
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		ctx.Set("user", user)
		ctx.Next()
		return
	}
}

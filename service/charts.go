package service

import (
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/now"
	"gitlab.com/kotahkon/utilities/database"
	"gitlab.com/kotahkon/utilities/model"
)

type Counter struct {
	Date  string `json:"date"`
	Count uint   `json:"count"`
}

func (s *Service) chartsLinkLineGetHandler(ctx *gin.Context) {
	code := ctx.Param("code")
	target := model.GetTarget("short_id", code)
	if target == nil {
		notFound(ctx)
		return
	}
	days, err := strconv.Atoi(ctx.Query("days"))
	if err != nil {
		days = 7
	}

	unique := ctx.Query("unique") == "true"

	user := s.getUserFromContext(ctx)
	if user.ID != target.CreatorID {
		forbidden(ctx, "Forbidden access")
		return
	}

	response := []Counter{}
	usersQuery := `SELECT * FROM visit_records`
	if unique {
		usersQuery = `SELECT DISTINCT ON (checksum) * FROM visit_records`
	}
	if days == 0 {
		database.Get().Raw(`
		  SELECT d.date, count(se.id) FROM (
			SELECT to_char(date_trunc('hour', offs), 'HH24:MI') AS date 
			FROM generate_series(current_date, current_timestamp, interval  '1 hour') AS offs
		  ) d LEFT OUTER JOIN (
			`+usersQuery+` WHERE visit_records.target_id = ?
		  ) se ON d.date = to_char(date_trunc('hour', se.created_at), 'HH24:MI') GROUP BY d.date ORDER BY d.date
		`, target.ID).Scan(&response)
	} else if days == 1 {
		database.Get().Raw(`
		SELECT d.date, count(se.id) FROM (
		  SELECT to_char(date_trunc('hour', offs), 'MM/DD HH24:MI') AS date 
		  FROM generate_series(current_timestamp - interval '1 day', current_timestamp, interval  '1 hour') AS offs
		) d LEFT OUTER JOIN (
			`+usersQuery+` WHERE visit_records.target_id = ?
		) se ON d.date = to_char(date_trunc('hour', se.created_at), 'MM/DD HH24:MI') GROUP BY d.date ORDER BY d.date
	  `, target.ID).Scan(&response)
	} else {
		database.Get().Raw(`SELECT d.date, count(se.id) FROM (
			SELECT to_char(date_trunc('day', (current_date - offs)), 'YYYY-MM-DD') AS date 
			FROM generate_series(0, ?, 1) AS offs
	  	) d LEFT OUTER JOIN (
			`+usersQuery+` WHERE visit_records.target_id = ?
	  	) se ON d.date = to_char(date_trunc('day', se.created_at), 'YYYY-MM-DD') 
	    GROUP BY d.date ORDER BY d.date;`, days, target.ID).Scan(&response)
	}

	success(ctx, response)
}

func (s *Service) chartsLinkPieGetHandler(ctx *gin.Context) {
	code := ctx.Param("code")
	target := model.GetTarget("short_id", code)
	if target == nil {
		notFound(ctx)
		return
	}
	days, err := strconv.Atoi(ctx.Query("days"))
	if err != nil {
		days = 7
	}

	user := s.getUserFromContext(ctx)
	if user.ID != target.CreatorID {
		forbidden(ctx, "Forbidden access")
		return
	}

	unique := ctx.Query("unique") == "true"

	var response struct {
		Platforms struct {
			Linux        int `json:"linux,omitempty"`
			Windows      int `json:"windows,omitempty"`
			Mac          int `json:"mac,omitempty"`
			Bot          int `json:"bot,omitempty"`
			IPad         int `json:"ipad,omitempty"`
			IPod         int `json:"ipod,omitempty"`
			IPhone       int `json:"iphone,omitempty"`
			Blackberry   int `json:"blackberry,omitempty"`
			WindowsPhone int `json:"windows_phone,omitempty"`
			Nintendo     int `json:"nintendo,omitempty"`
			Unknown      int `json:"unknown,omitempty"`
			Playstation  int `json:"playstation,omitempty"`
		} `json:"platforms"`
		Browsers struct {
			Chrome  int `json:"chrome,omitempty"`
			Safari  int `json:"safari,omitempty"`
			Opera   int `json:"opera,omitempty"`
			Firefox int `json:"firefox,omitempty"`
			Other   int `json:"other,omitempty"`
		} `json:"browsers"`
		Devices struct {
			Mobile  int `json:"mobile"`
			Desktop int `json:"desktop"`
			Other   int `json:"other,omitempty"`
		} `json:"devices"`
	}

	table := "visit_records"
	if unique {
		table = "(SELECT DISTINCT ON (checksum) * FROM visit_records)"
	}

	var (
		fromTime time.Time
		toTime   time.Time
	)
	if days == 0 {
		fromTime = now.BeginningOfDay()
		toTime = time.Now()
	} else {
		fromTime = now.BeginningOfDay().Add(-24 * time.Duration(days) * time.Hour)
		toTime = now.BeginningOfDay()
	}

	database.Get().Raw(`
		SELECT 
			COUNT(vr.id) filter (where platform = 'Windows') as Windows, 
			COUNT(vr.id) filter (where platform = 'Mac') as Mac,
			COUNT(vr.id) filter (where platform = 'Linux') as Linux,
			COUNT(vr.id) filter (where platform = 'iPad') as iPad,
			COUNT(vr.id) filter (where platform = 'iPhone') as iPhone,
			COUNT(vr.id) filter (where platform = 'iPod') as iPod,
			COUNT(vr.id) filter (where platform = 'Blackberry') as Blackberry,
			COUNT(vr.id) filter (where platform = 'WindowsPhone') as WindowsPhone,
			COUNT(vr.id) filter (where platform = 'Playstation') as Playstation,
			COUNT(vr.id) filter (where platform = 'Nintendo') as Nintendo,
			COUNT(vr.id) filter (where platform = 'Bot') as Bot,
			COUNT(vr.id) filter (where platform = 'Unknown') as Unknown
		FROM `+table+` vr
		where vr.target_id = ? AND vr.created_at BETWEEN ? AND ?;`, target.ID, fromTime, toTime).Scan(&response.Platforms)

	database.Get().Raw(`
		SELECT
			COUNT(vr.id) filter (where device_type = 'Phone') as Mobile,
			COUNT(vr.id) filter (where device_type = 'Computer') as Desktop,
			COUNT(vr.id) filter (where device_type NOT IN ('Phone', 'Computer')) as Other
		FROM `+table+` vr
		where vr.target_id = ? AND vr.created_at BETWEEN ? AND ?;`, target.ID, fromTime, toTime).Scan(&response.Devices)

	database.Get().Raw(`
		SELECT 
			COUNT(vr.id) filter (where browser_name = 'Opera') as Opera, 
			COUNT(vr.id) filter (where browser_name = 'Chrome') as Chrome,
			COUNT(vr.id) filter (where browser_name = 'Firefox') as Firefox,
			COUNT(vr.id) filter (where browser_name = 'Safari') as Safari,
			COUNT(vr.id) filter (where browser_name NOT IN ('Safari', 'Chrome', 'Firefox', 'Opera')) as Other
		FROM `+table+` vr
		where vr.target_id = ? AND vr.created_at BETWEEN ? AND ?;`, target.ID, fromTime, toTime).Scan(&response.Browsers)

	success(ctx, response)
}

func (s *Service) chartsOverallGetHandler(ctx *gin.Context) {
	user := s.getUserFromContext(ctx)
	days, err := strconv.Atoi(ctx.Query("days"))
	if err != nil {
		days = 7
		return
	}

	output := []Counter{}
	database.Get().Raw(`SELECT d.date, count(se.id) FROM (
		SELECT to_char(date_trunc('day', (current_date - offs)), 'YYYY-MM-DD') AS date 
		FROM generate_series(0, ?, 1) AS offs
	  ) d LEFT OUTER JOIN (
		SELECT visit_records.* FROM targets, visit_records WHERE visit_records.target_id = targets.id and targets.creator_id = ?
	  ) se ON d.date = to_char(date_trunc('day', se.created_at), 'YYYY-MM-DD') GROUP BY d.date ORDER BY d.date;`, days-1, user.ID).Scan(&output)

	success(ctx, gin.H{
		"output": output,
	})
}

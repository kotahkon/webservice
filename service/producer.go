package service

import (
	"github.com/nsqio/go-nsq"
)

var (
	producer *nsq.Producer
)

func newProducer(nsqConnection string) error {
	p, err := nsq.NewProducer(nsqConnection, nsq.NewConfig())
	if err != nil {
		return err
	}
	producer = p
	return p.Ping()
}

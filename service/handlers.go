package service

import (
	"net/http"
	"time"

	"github.com/dpapathanasiou/go-recaptcha"
	"github.com/gin-gonic/gin"
	"github.com/vmihailenco/msgpack"

	"gitlab.com/kotahkon/utilities/database"
	"gitlab.com/kotahkon/utilities/model"
	"gitlab.com/kotahkon/utilities/validator"
	"gitlab.com/kotahkon/webservice/service/secure"
)

func (s *Service) authLoginPostHandler(ctx *gin.Context) {
	var input struct {
		Username string `json:"username"`
		Password string `json:"password"`
		Captcha  string `json:"captcha"`
	}
	err := ctx.BindJSON(&input)
	if err != nil {
		failed(ctx, "Bad JSON Input", err)
		return
	}
	confirmed, err := recaptcha.Confirm(ctx.ClientIP(), input.Captcha)
	if err != nil {
		internalServerError(ctx, err)
		return
	}
	if !confirmed {
		forbidden(ctx, "Captcha is invalid")
		return
	}

	if !validator.Password(input.Password) {
		failed(ctx, "Invalid username or password")
		return
	}

	user := &database.User{}
	// TODO: SQL Injection
	if validator.Email(input.Username) {
		database.Model(user).Find(user, "email = ? AND password = ?", input.Username, secure.HashPassword(input.Password))
	} else if validator.Username(input.Username) {
		database.Model(user).Find(user, "username = ? AND password = ?", input.Username, secure.HashPassword(input.Password))
	} else {
		failed(ctx, "Invalid username or password")
	}
	if !user.Exists() {
		failed(ctx, "Invalid username or password")
		return
	}
	model.SetUser(user)

	// refreshToken.
	success(ctx, gin.H{
		"access_token": s.jwtGenerate(user.UID),
		"user":         user,
	})
}

func (s *Service) authRegisterPostHandler(ctx *gin.Context) {
	var input struct {
		Email    string `json:"email"`
		Username string `json:"username"`
		Password string `json:"password"`
		Captcha  string `json:"captcha"`
	}
	err := ctx.BindJSON(&input)
	if err != nil {
		failed(ctx, "Bad JSON Input", err)
		return
	}
	confirmed, err := recaptcha.Confirm(ctx.ClientIP(), input.Captcha)
	if err != nil {
		internalServerError(ctx, err)
		return
	}
	if !confirmed {
		forbidden(ctx, "Captcha is invalid")
		return
	}

	if !validator.Email(input.Email) {
		failed(ctx, "Bad email address")
		return
	}

	if !validator.Username(input.Username) {
		failed(ctx, "Only alphanumerical values with more than 4 characters are valid")
		return
	}

	if !validator.Password(input.Password) {
		failed(ctx, "Password length must be greater than 6 characters")
		return
	}

	user := &database.User{}
	// TODO: SQL Injection
	user.Email = input.Email
	user.Username = input.Username
	user.Password = secure.HashPassword(input.Password)
	err = database.Model(user).Create(user).Error
	if err != nil {
		failed(ctx, "Bad Datbase Error", err)
		return
	}
	model.SetUser(user)

	success(ctx, gin.H{
		"access_token": s.jwtGenerate(user.UID),
		"user":         user,
	})
}

func (s *Service) redirectGetHandler(ctx *gin.Context) {
	code := ctx.Param("code")
	target := model.GetTarget("short_id", code)
	if target == nil {
		ctx.String(http.StatusNotFound, "request not found")
		return
	}

	bytes, _ := msgpack.Marshal(map[string]interface{}{
		"target_id":  target.ID,
		"ip_address": ctx.ClientIP(),
		"user_agent": ctx.GetHeader("User-Agent"),
		"created_at": time.Now(),
	})
	producer.Publish("visit", bytes)

	redirect(ctx, target.TargetURL)
}

package secure

import (
	"crypto/sha256"
	"encoding/hex"
	"math/rand"
	"os"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func sha256hash(text string) string {
	hasher := sha256.New()
	hasher.Write([]byte(text))
	return hex.EncodeToString(hasher.Sum(nil))
}

func HashPassword(password string) string {
	return sha256hash(os.Getenv("SECRET_KEY") + "t01" + password)
}

func CheckPasswordHash(password, hash string) bool {
	return HashPassword(password) == hash
}
